require 'csv'

data = CSV.read(ARGV[0])
data2 = data[1..]

IMPORT = 0.05
BASIC = 0.1

def pre_tax(quantity, price)
	p = quantity.to_f() * price.to_f()
	return p
end

def import_tax(quantity, price)
	p = pre_tax(quantity, price) * IMPORT
	return p
end

def basic_tax(quantity, good, price)
	if good == "book" || good == "foods" || good = "medical products"
		return 0
	else
		p = pre_tax(quantity, price) * BASIC
		return p
	end
end

def sales_tax_helper(quantity, good, price)
	p = import_tax(quantity, price) + basic_tax(quantity, good, price)
	return p.round(2)
end 

def sales_tax(array)
	return sales_tax_helper(array[0], array[1], array[2])
end

tax = 0
for a in data2
	tax += sales_tax(a)
end
total = 0
for a in data2
	total += a[2].to_f
end

		

output = CSV.open(ARGV[1], "w") do |csv|
		for d in data2
			csv << d
		end
		csv << []
		csv << ["Sales Taxes: "+tax.round(2).to_s]
		csv << ["Total: " + total.round(2).to_s]
	end

puts output

